variable "aws_region" {
  type        = string
  description = "The AWS region."
}
variable "ec2_iam_role_name" {
  type        = string
  description = "The EC2's IAM role name."
}
variable "aws_s3_log_bucket" {
  type        = string
  description = "The S3 bucket name to store image builder logs."
}
variable "aws_image_builder_components_s3_bucket" {
  type        = string
  description = "The S3 bucket name that stores the Image Builder component files."
}
variable "ebs_root_vol_size" {
  type = number
}
variable "image_pipeline_version" {
  type = string
}
variable "ami_name_tag" {
  type        = string
  description = "The name to give the AMI that is successfully produced by the image builder pipeline"
}
variable "default_tags" {
  description = "AWS tags to apply to all resources created by this terraform module"
  type        = map(any)
}
variable "image_pipeline_status" {
  description = "Pipeline status: ENABLED or DISABLED"
  type        = string

  validation {
    condition     = var.image_pipeline_status == "ENABLED" || var.image_pipeline_status == "DISABLED"
    error_message = "ERROR: image_pipeline_status variable must be one of 'ENABLED' or 'DISABLED'"
  }
}

#   "image-builder:version" = var.image_pipeline_version
