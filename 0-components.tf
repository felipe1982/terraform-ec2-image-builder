resource "aws_s3_object" "this" {
  for_each = fileset("/files/", "*")

  bucket = var.aws_image_builder_components_s3_bucket
  key    = "/files/${each.value}"
  source = "/files/${each.value}"
  # If the md5 hash is different it will re-upload
  etag = filemd5("/files/${each.value}")
}

resource "aws_kms_key" "image_builder" {}

resource "aws_kms_alias" "image_builder" {
  name          = "alias/image_builder"
  target_key_id = aws_kms_key.image_builder.key_id
}
