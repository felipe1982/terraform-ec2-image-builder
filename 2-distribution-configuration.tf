# This fine determines how the AMI is distributed after it has been built and
# tested.
#
resource "aws_imagebuilder_distribution_configuration" "this" {
  name = "local-distribution"

  distribution {
    ami_distribution_configuration {
      ami_tags = var.default_tags
      name     = "PingDataSync-{{ imagebuilder:buildDate }}"

      launch_permission {
        # user_ids = ["123456789012"]
      }
    }
    region = var.aws_region
  }
}
