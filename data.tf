data "aws_region" "current" {}
data "aws_partition" "current" {}

data "aws_subnet" "this" {
  filter {
    name   = "tag:Name"
    values = ["virgin-innovation-dev-trevor-subnet-public1-ap-southeast-2a"]
  }
}

data "aws_security_group" "this" {
  filter {
    name   = "tag:Name"
    values = ["virgin-innovation-dev-trevor-PDLocal-SG"]
  }
}
