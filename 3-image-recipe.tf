# An EC2 Image Builder recipe defines the base image to use as your starting
# point to create a new image, along with the set of components that you add to
# customize your image and verify that everything is working as expected. A
# maximum of 20 components, which include build and test, can be applied to a
# recipe.

resource "aws_imagebuilder_image_recipe" "this" {
  name         = "ping-data-sync-recipe"
  parent_image = "arn:${data.aws_partition.current.partition}:imagebuilder:${data.aws_region.current.name}:aws:image/amazon-linux-2-x86/x.x.x"
  version      = var.image_pipeline_version
  block_device_mapping {
    device_name = "/dev/xvdb"
    ebs {
      delete_on_termination = true
      volume_size           = var.ebs_root_vol_size
      volume_type           = "gp3"
    }
  }
  component {
    component_arn = "arn:aws:imagebuilder:ap-southeast-2:aws:component/amazon-cloudwatch-agent-linux/x.x.x"
  }
  component {
    component_arn = "arn:aws:imagebuilder:ap-southeast-2:aws:component/aws-cli-version-2-linux/x.x.x"
  }
  component {
    component_arn = aws_imagebuilder_component.hello-world.arn
  }
  lifecycle {
    create_before_destroy = true
  }
}
