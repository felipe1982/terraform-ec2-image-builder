terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  default_tags {
    tags = var.default_tags
  }
  region = var.aws_region
}

terraform {
  backend "s3" {}
}
