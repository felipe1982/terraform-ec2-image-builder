ec2_iam_role_name = "svc-image-builder-role"
ebs_root_vol_size = 10
ami_name_tag      = "amazon-linux-2"


aws_region                             = "ap-southeast-2"
aws_s3_log_bucket                      = "versent-innovation-felipe"
aws_image_builder_components_s3_bucket = "versent-innovation-felipe"
image_pipeline_version                 = "1.0.4" # increment eachimage_pipeline_status
image_pipeline_status                  = "DISABLED"

default_tags = {
  # --- network tags --- #
  "service:contact"       = "cloudops@virginaustralia.com"
  "service:owner"         = "Simon Lawrence"
  "service:name"          = "aws-network"
  "billing:costcode"      = "A123"
  "git:url"               = "https://gitlab.com/virginaustralia/infrastructure/terraform-ping-data-sync-image-builder.git"
  "service:domain"        = "ts"
  "sevice:env"            = "npr"

  # -- Account tags -- #
  #   "platform:id"      = ""
  #   "platform:domain"  = ""
  #   "platform:type"    = ""
  #   "platform:env"     = ""
}
