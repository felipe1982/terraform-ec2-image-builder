# You can use infrastructure configurations to specify the Amazon EC2
# infrastructure that Image Builder uses to build and test your EC2 Image
# Builder image. Infrastructure settings include:

# - Instance types for your build and test infrastructure. We recommend that you
#   specify more than one instance type because this allows Image Builder to
#   launch an instance from a pool with sufficient capacity. This can reduce
#   your transient build failures.

# - An instance profile that provides your build and test instances with the
#   permissions that are required to perform customization activities. For
#   example, if you have a component that retrieves resources from Amazon S3,
#   the instance profile requires permissions to access those files. The
#   instance profile also requires a minimal set of permissions for EC2 Image
#   Builder to successfully communicate with the instance. For more information,
#   see Prerequisites.

# - The VPC, subnet, and security groups for your pipeline's build and test
#   instances.

# - The Amazon S3 location where Image Builder stores application logs from your
#   build and testing. If you configure logging, the instance profile specified
#   in your infrastructure configuration must have s3:PutObject permissions for
#   the target bucket (arn:aws:s3:::BucketName/*).

# - An Amazon EC2 key pair that allows you to log on to your instance to
#   troubleshoot if your build fails and you set terminateInstanceOnFailure to
#   false.

# - An SNS topic to which Image Builder sends event notifications.

resource "aws_imagebuilder_infrastructure_configuration" "this" {
  description                   = "Simple infrastructure configuration"
  instance_profile_name         = var.ec2_iam_role_name
  instance_types                = ["m5a.large"]
  name                          = "ping-data-sync-build-infrastructure"
  security_group_ids            = [data.aws_security_group.this.id]
  subnet_id                     = data.aws_subnet.this.id
  terminate_instance_on_failure = true

  logging {
    s3_logs {
      s3_bucket_name = var.aws_s3_log_bucket
      s3_key_prefix  = "image-builder"
    }
  }

  tags = {
    Name = "ping-data-sync-build-infrastructure"
  }
}
